//
// Created by David on 11.11.2019.
//
#pragma once
#include <string>

class IObject
{
public:
    virtual std::string toString() const = 0;
};