//
// Created by david on 11.11.2019.
//
#pragma once
#include "IObject.h"

class IComparable : public IObject
{
public:
    virtual int compareTo(IComparable* aObject) const = 0;
};
