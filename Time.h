//
// Created by david on 11.11.2019.
//
#pragma once
#include "IComparable.h"

class Time : public IComparable
{
    private:
        int _hours;
        int _minutes;
        int _seconds;
    public:
        Time(int aHours, int aMinutes, int aSeconds);
        virtual std::string toString() const;
        virtual int compareTo(IComparable* obj) const;
};
