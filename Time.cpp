//
// Created by david on 11.11.2019.
//
#include <stdexcept>
#include "Time.h"

Time::Time(int aHours, int aMinutes, int aSeconds) {
    if (aHours >= 0 && aHours < 24 && aMinutes >= 0 && aMinutes < 60 && aSeconds >= 0 && aSeconds < 60) {
        _hours = aHours;
        _minutes = aMinutes;
        _seconds = aSeconds;
    }
    else {
        throw std::invalid_argument("Invalid argument exception.");
    }
}

std::string Time::toString() const
{
    return "Time: " + std::to_string(_hours) + ":"
           + std::to_string(_minutes) + ":"
           + std::to_string(_seconds);
}

int Time::compareTo(IComparable* object) const
{
    Time* time = dynamic_cast<Time*>(object);
    if(time != nullptr){
        throw std::invalid_argument("Argument type is not TIME!");
    }
    int superValue = this->_hours * 3600 + this->_minutes * 60 + this->_seconds;
    int compareToValue = time->_hours * 3600 + time->_minutes * 60 + time->_seconds;

    if (superValue == compareToValue) return 0; //Same value
    else if (superValue > compareToValue) return 1; // > -> 1
    else return -1; // (< -> -1
}

